﻿using System.Linq;
using System.Text.RegularExpressions;
using NUnit.Framework;
using Woody.ML.Model;
using UnityEngine;

namespace Tests
{
    public class ImageClassifierTest
    {
        // A Test behaves as an ordinary method
        [Test]
        public void ImageClassifierTestSimplePasses()
        {
            var asset = Resources.Load("woody_tf17") as TextAsset;
            var labels = Resources.Load("woody_labels") as TextAsset;

            var classifier = new Classifier(asset.bytes,
                Regex.Split(labels.text, "\n|\r|\r\n")
                    .Where(s => !string.IsNullOrEmpty(s)).ToArray(),
                224);         
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TensorFlow;

namespace Woody.ML.Model
{
    public class FullClassifier
    {

        private static readonly string INPUT_NAME = "dense_1_input";
        private static readonly string OUTPUT_NAME = "dense_3/BiasAdd";
        private readonly TFGraph _graph;
        private readonly string[] _labels;


        public FullClassifier(byte[] model, string[] labels)
        {
#if UNITY_ANDROID
                  TensorFlowSharp.Android.NativeBinding.Init();
#endif
            _labels = labels;
            _graph = new TFGraph();
            _graph.Import(model, "");

        }

        public Task<string> ClassifyFull(float[] probabilities)
        {
            var final = "some string";
            var final_list = new List<KeyValuePair<string, float>>();

            return Task.Run(() =>
                {
                    using (var session = new TFSession(_graph))

                    using (var tensor = TransformFloats(probabilities))
                    {
                        var runner = session.GetRunner();
                        runner.AddInput(_graph[INPUT_NAME][0], tensor).Fetch(_graph[OUTPUT_NAME][0]);
                        var output = runner.Run();
                        var result = output[0];
                        var probs = ((float[][]) result.GetValue(true))[0];

                        for (var i = 0; i < _labels.Length; i++)
                            final_list.Add(new KeyValuePair<string, float>(_labels[i], probs[i] * 100));
                        var prep = final_list.OrderByDescending(x => x.Value).ToList();

                        final = prep[0].Key;

                        foreach (var ts in output) ts.Dispose();
                    }

                    return final;
                }
            );
        }


        /// <summary>
        /// Transforms the predictions done by the image classifier to a TFTensor object.
        /// </summary>
        /// <param name="floats"></param>
        /// <returns></returns>
        private static TFTensor TransformFloats(float[] floats)
        {
            var shape = new TFShape(1, 32);
            return TFTensor.FromBuffer(shape, floats, 0, floats.Length);
        }
    }
}
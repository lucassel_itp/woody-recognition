using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TensorFlow;
using UnityEngine;

namespace Woody.ML.Model
{
    public class Classifier
    {
        private static readonly string INPUT_NAME = "input_1";
        private static readonly string OUTPUT_NAME = "dense_3/Sigmoid";
        private readonly TFGraph _graph;
        private readonly int _inputSize;
        private readonly string[] _labels;

        /// <summary>
        /// Image classifier constructor.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="labels"></param>
        /// <param name="inputSize"></param>
        public Classifier(byte[] model, string[] labels, int inputSize)
        {
#if UNITY_ANDROID
            TensorFlowSharp.Android.NativeBinding.Init();
#endif
            _labels = labels;
            _inputSize = inputSize;
            _graph = new TFGraph();
            _graph.Import(model, "");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Task<List<KeyValuePair<string, float>>> ClassifyAsync(Color32[] data)
        {
            return Task.Run(() =>
            {
                var map = new List<KeyValuePair<string, float>>();

                using (var session = new TFSession(_graph))

                using (var tensor = TransformInput(data, _inputSize, _inputSize))
                {
                    var runner = session.GetRunner();
                    runner.AddInput(_graph[INPUT_NAME][0], tensor).Fetch(_graph[OUTPUT_NAME][0]);
                    var output = runner.Run();

                    // output[0].Value() is a vector containing probabilities of
                    // labels for each image in the "batch". The batch size was 1.
                    // Find the most probably label index.

                    var result = output[0];
                    var rshape = result.Shape;

                    if (result.NumDims != 2 || rshape[0] != 1)
                    {
                        var shape = "";
                        foreach (var d in rshape) shape += $"{d} ";

                        shape.Trim();

                        Debug.Log(
                            "Error: expected to produce a [1 N] shaped tensor where N is the number of labels, instead it produced one with shape [{shape}]");
                        Environment.Exit(1);
                    }

                    var probabilities = ((float[][]) result.GetValue(true))[0];

                    for (var i = 0; i < _labels.Length; i++)
                        map.Add(new KeyValuePair<string, float>(_labels[i], probabilities[i]));

                    foreach (var ts in output) ts.Dispose();
                }

                return map;
            });
        }

        /// <summary>
        /// Transforms a Color32[] into a TFTensor object. Colors should be in 0f -> 1f range.
        /// </summary>
        /// <param name="pic"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <returns></returns>
        private static TFTensor TransformInput(Color32[] pic, int width, int height)
        {

            var floatValues = new float[width * height * 3];

            for (var i = 0; i < pic.Length; ++i)
            {
                floatValues[i * 3 + 0] = pic[i].r / 255f;
                floatValues[i * 3 + 1] = pic[i].g / 255f;
                floatValues[i * 3 + 2] = pic[i].b / 255f;
            }

            var shape = new TFShape(1, width, height, 3);

            return TFTensor.FromBuffer(shape, floatValues, 0, floatValues.Length);
        }
    }
}
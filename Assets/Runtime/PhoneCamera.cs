﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TensorFlow;
using Woody.ML.Model;
using UnityEngine;
using UnityEngine.UI;

public enum Device
{
    Mobile,
    Standalone
}

namespace Woody.ML.Runtime
{
    public class PhoneCamera : MonoBehaviour
    {
        private const int classifyImageSize = 224;

        private WebCamTexture deviceCamera;
        public RawImage background;
        private Vector2 backgroundOrigin;
        private bool camAvailable;
        public bool frontCam;

        private Classifier classifier;
        private FullClassifier _fullyConnectedClassifier;

        private float[] _fullInput;
        private float[,] _buffer;

        private Device Device;

        public AspectRatioFitter fitter;

        [Header("Configuration")] public bool FullClassification;

        [Header("Fully connected")] public TextAsset FullyConnected;
        public TextAsset ConnectedLabelsFile;

        [Header("Image classifier")] public TextAsset ImageClassifier;
        public TextAsset ClassifierLabelsFile;

        [Header("UI")] public Text uiText;
        public Text finalText;

        private void Start()
        {
            _fullInput = new float[32];
            _buffer = new float[5, 32];
            LoadModels();

            if (Application.isEditor)
            {
                deviceCamera = new WebCamTexture();
                deviceCamera.Play();
                camAvailable = true;
            }
            else
                SelectCamera(frontCam);
            

            if (!camAvailable)
            {
                return;
            }
            
            background.texture = deviceCamera;
            

            //InvokeRepeating(nameof(TFClassify), 1f, 1f);
        }


        private void Update()
        {
            if (!camAvailable) return;

            var ratio = (float)deviceCamera.width / (float)deviceCamera.height;
            fitter.aspectRatio = ratio;

            var scaleY = deviceCamera.videoVerticallyMirrored ? -1f : 1f;
            background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

            var orient = -deviceCamera.videoRotationAngle;
            background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
        }


        private void LoadModels()
        {
            try
            {
                LoadClassifier();
                
                if (FullClassification) LoadFullyConnected();
            }
            catch (TFException ex)
            {
                if (ex.Message.EndsWith("is up to date with your GraphDef-generating binary.)."))

                {
                    const string error = "Error: TFException. Make sure you model trained with same version of TensorFlow as in Unity plugin.";
                    uiText.text = error;
                    Debug.LogError(error);
                }
                   
                        

                throw;
            }
        }


        private void LoadClassifier()
        {
            classifier = new Classifier(
                ImageClassifier.bytes,
                Regex.Split(ClassifierLabelsFile.text, "\n|\r|\r\n")
                    .Where(s => !string.IsNullOrEmpty(s)).ToArray(),
                classifyImageSize);
        }

        private void LoadFullyConnected()
        {
            _fullyConnectedClassifier = new FullClassifier(
                FullyConnected.bytes,
                Regex.Split(ConnectedLabelsFile.text, "\n|\r|\r\n")
                    .Where(s => !string.IsNullOrEmpty(s)).ToArray());
        }


        public async void Classify()
        {
            var snap = TakeTextureSnap();
            var scaled = Scale(snap, classifyImageSize);
            var rotated = await RotateAsync(scaled.GetPixels32(), scaled.width, scaled.height);
            
            var snap2 = TakeTextureSnap();
            var scaled2 = Scale(snap2, classifyImageSize);
            var rotated2 = await RotateAsync(scaled2.GetPixels32(), scaled2.width, scaled2.height);

            var snap3 = TakeTextureSnap();
            var scaled3 = Scale(snap3, classifyImageSize);
            var rotated3 = await RotateAsync(scaled3.GetPixels32(), scaled3.width, scaled3.height);

            var snap4 = TakeTextureSnap();
            var scaled4 = Scale(snap4, classifyImageSize);
            var rotated4 = await RotateAsync(scaled4.GetPixels32(), scaled4.width, scaled4.height);

            var snap5 = TakeTextureSnap();
            var scaled5 = Scale(snap5, classifyImageSize);
            var rotated5 = await RotateAsync(scaled5.GetPixels32(), scaled5.width, scaled5.height);


            try
            {
                var probabilities = await classifier.ClassifyAsync(rotated);
                var probabilities2 = await classifier.ClassifyAsync(rotated2);
                var probabilities3 = await classifier.ClassifyAsync(rotated3);
                var probabilities4 = await classifier.ClassifyAsync(rotated4);
                var probabilities5 = await classifier.ClassifyAsync(rotated5);
                
                
                var finalList = probabilities.OrderByDescending(x => x.Value).ToList();
                
                
                if (FullClassification)
                {
                    Array.Clear(_fullInput, 0, _fullInput.Length);

                    var results = new float[32];
                    var results2 = new float[32];
                    var results3 = new float[32];
                    var results4 = new float[32];
                    var results5 = new float[32];
                    
                    for (var i = 0; i < probabilities.Count; i++)
                    {
                        results[i] = probabilities[i].Value;
                        results2[i] = probabilities2[i].Value;
                        results3[i] = probabilities3[i].Value;
                        results4[i] = probabilities4[i].Value;
                        results5[i] = probabilities5[i].Value;
                    }

                    for (var i = 0; i < probabilities.Count; i++)
                    {
                        _fullInput[i] = (results[i] + results2[i] + results3[i] + results4[i] + results5[i]) / 5;
                    }
                    
                   
                    try
                    {
                        var final = await _fullyConnectedClassifier.ClassifyFull(_fullInput);
                        
                        finalText.text = final.ToUpper();

                        uiText.text = string.Empty;
                        
                        for (var i = 0; i < 3; i++)
                            uiText.text += finalList[i].Key + ": " + $"{finalList[i].Value * 100f :0.000}%" + "\n";
                    }
                    catch (NullReferenceException)
                    {
                        Debug.Log("Error: Full model processinging failed.");
                        throw;
                    }
                }
                else
                {
                    uiText.text = string.Empty;

                    for (var i = 0; i < 3; i++)
                        uiText.text += finalList[i].Key + ": " + $"{finalList[i].Value:0.000}%" + "\n";
                }
            }
            catch (NullReferenceException)
            {
                Debug.Log("Error: TensorFlow NullReferenceException. Make sure you set correct INPUT_NAME and OUTPUT_NAME");
            }
            finally
            {
                if (Application.isEditor)
                {
                    DestroyImmediate(snap);
                    DestroyImmediate(scaled);
                }
                else
                {
                    Destroy(snap);
                    Destroy(scaled);
                }
            }
        }

        private void SelectCamera(bool front)
        {                       

            var devices = WebCamTexture.devices;
            
            if (devices.Length == 0)
            {
                const string error = "No camera's detected";
                uiText.text = error;
                Debug.LogError(error);
                camAvailable = false;
                return;
            }
            
            if (front)
            {
                for (var i = 0; i < devices.Length; i++)
                    if (devices[i].isFrontFacing)
                        deviceCamera = new WebCamTexture(devices[i].name, Screen.width, Screen.height);

                if (deviceCamera == null)
                {
                    const string error = "Unable to find front camera";
                    uiText.text = error;
                    Debug.LogWarning(error);
                }
                else
                {
                    Debug.Log("front camera found");
                    deviceCamera.Play();
                    camAvailable = true;
                }
            }
            else
            {
                for (var i = 0; i < devices.Length; i++)
                    if (!devices[i].isFrontFacing)
                        deviceCamera = new WebCamTexture(devices[i].name, Screen.width, Screen.height);

                if (deviceCamera == null)
                {
                    const string error = "Unable to find back camera";
                    uiText.text = error;
                    Debug.LogWarning(error);
                }
                else
                {
                    Debug.Log("back camera found");
                    deviceCamera.Play();
                    camAvailable = true;
                }
            }
           
        }

        

        public void SwitchCamera()
        {
            Debug.Log("switching camera");
            camAvailable = false;
            deviceCamera.Stop();
            deviceCamera = null;
            frontCam = !frontCam;
            SelectCamera(frontCam);
        }


        private Texture2D TakeTextureSnap()
        {
            var smallest = deviceCamera.width < deviceCamera.height ? deviceCamera.width : deviceCamera.height;
            var snap = TextureTools.CropWithRect(deviceCamera,
                new Rect(0, 0, smallest, smallest),
                TextureTools.RectOptions.Center, 0, 0);

            return snap;
        }

        private static Texture2D Scale(Texture2D texture, int imageSize)
        {
            var scaled = TextureTools.scaled(texture, imageSize, imageSize, FilterMode.Bilinear);
            return scaled;
        }


        private Task<Color32[]> RotateAsync(Color32[] pixels, int width, int height)
        {
            return Task.Run(() => TextureTools.RotateImageMatrix(
                pixels, width, height, -90));
        }
    }
}